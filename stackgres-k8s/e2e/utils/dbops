#!/bin/sh

assert_dbops_running() {
  notrace_function traceable_assert_dbops_running "$@"
}

traceable_assert_dbops_running() {
  local DBOPS_NAME="$1"
  local CLUSTER_NAMESPACE="${2:-$CLUSTER_NAMESPACE}"
  local E2E_TIMEOUT="${3:-$E2E_TIMEOUT}"
  local OP
  OP="$(kubectl get sgdbops -n "$CLUSTER_NAMESPACE" "$DBOPS_NAME" --template '{{ .spec.op }}' || echo unknown)"

  if [ "$OP" = unknown ]
  then
    echo
    echo "SGDbOps $CLUSTER_NAMESPACE.$DBOPS_NAME not found"
    echo
    return 1
  fi

  OP="$(printf '%s' "$OP" | sed 's/\([A-Z]\)/ \1/g' | tr '[A-Z]' '[a-z]')"

  local START
  START="$(date +%s)"
  while true
  do
    if kubectl wait --timeout 1s -n "$CLUSTER_NAMESPACE" sgdbops "$DBOPS_NAME" \
      --for condition=Running 2>/dev/null
    then
      echo "SUCCESS. $OP operation $CLUSTER_NAMESPACE.$DBOPS_NAME running."
      return
    fi
    if kubectl wait --timeout 1s -n "$CLUSTER_NAMESPACE" sgdbops "$DBOPS_NAME" \
      --for condition=Completed 2>/dev/null
    then
      echo "SUCCESS. $OP operation $CLUSTER_NAMESPACE.$DBOPS_NAME completed."
      return
    fi
    if kubectl wait --timeout 1s -n "$CLUSTER_NAMESPACE" sgdbops "$DBOPS_NAME" \
      --for condition=Failed 2>/dev/null
    then
      echo "FAILED. $OP operation $CLUSTER_NAMESPACE.$DBOPS_NAME failed."
      echo
      echo "Job logs:"
      echo
      kubectl logs -n "$CLUSTER_NAMESPACE" \
        -l "app=StackGresDbOps,dbops-name=$DBOPS_NAME,db-ops=true" \
        --all-containers --ignore-errors --tail=-1
      echo
      return 1
    fi
    if [ "$((START + E2E_TIMEOUT))" -le "$(date +%s)" ]
    then
      echo
      echo "FAILED. $OP operation $CLUSTER_NAMESPACE.$DBOPS_NAME was not running after $E2E_TIMEOUT seconds"
      echo
      echo "Job logs:"
      echo
      kubectl logs -n "$CLUSTER_NAMESPACE" \
        -l "app=StackGresDbOps,dbops-name=$DBOPS_NAME,db-ops=true" \
        --all-containers --ignore-errors --tail=-1
      echo
      return 1
    fi
  done
}

assert_dbops_completion() {
  notrace_function traceable_assert_dbops_completion "$@"
}

traceable_assert_dbops_completion() {
  local DBOPS_NAME="$1"
  local CLUSTER_NAMESPACE="${2:-$CLUSTER_NAMESPACE}"
  local E2E_TIMEOUT="${3:-$E2E_TIMEOUT}"
  local OP
  OP="$(kubectl get sgdbops -n "$CLUSTER_NAMESPACE" "$DBOPS_NAME" --template '{{ .spec.op }}' || echo unknown)"

  if [ "$OP" = unknown ]
  then
    echo
    echo "SGDbOps $CLUSTER_NAMESPACE.$DBOPS_NAME not found"
    echo
    return 1
  fi

  OP="$(printf '%s' "$OP" | sed 's/\([A-Z]\)/ \1/g' | tr '[A-Z]' '[a-z]')"

  local START
  START="$(date +%s)"
  while true
  do
    if kubectl wait --timeout 1s -n "$CLUSTER_NAMESPACE" sgdbops "$DBOPS_NAME" \
      --for condition=Completed 2>/dev/null
    then
      echo "SUCCESS. $OP operation $CLUSTER_NAMESPACE.$DBOPS_NAME completed."
      return
    fi
    if kubectl wait --timeout 1s -n "$CLUSTER_NAMESPACE" sgdbops "$DBOPS_NAME" \
      --for condition=Failed 2>/dev/null
    then
      echo "FAILED. $OP operation $CLUSTER_NAMESPACE.$DBOPS_NAME failed."
      echo
      echo "Job logs:"
      echo
      kubectl logs -n "$CLUSTER_NAMESPACE" \
        -l "app=StackGresDbOps,dbops-name=$DBOPS_NAME,db-ops=true" \
        --all-containers --ignore-errors --tail=-1
      echo
      return 1
    fi
    if [ "$((START + E2E_TIMEOUT))" -le "$(date +%s)" ]
    then
      echo
      echo "FAILED. $OP operation $CLUSTER_NAMESPACE.$DBOPS_NAME did not complete after $E2E_TIMEOUT seconds"
      echo
      echo "Job logs:"
      echo
      kubectl logs -n "$CLUSTER_NAMESPACE" \
        -l "app=StackGresDbOps,dbops-name=$DBOPS_NAME,db-ops=true" \
        --all-containers --ignore-errors --tail=-1
      echo
      return 1
    fi
  done
}

assert_dbops_failure() {
  notrace_function traceable_assert_dbops_failure "$@"
}

traceable_assert_dbops_failure() {
  local DBOPS_NAME="$1"
  local CLUSTER_NAMESPACE="${2:-$CLUSTER_NAMESPACE}"
  local E2E_TIMEOUT="${3:-$E2E_TIMEOUT}"
  local OP
  OP="$(kubectl get sgdbops -n "$CLUSTER_NAMESPACE" "$DBOPS_NAME" --template '{{ .spec.op }}' || echo unknown)"

  if [ "$OP" = unknown ]
  then
    echo
    echo "SGDbOps $CLUSTER_NAMESPACE.$DBOPS_NAME not found"
    echo
    return 1
  fi

  OP="$(printf '%s' "$OP" | sed 's/\([A-Z]\)/ \1/g' | tr '[A-Z]' '[a-z]')"

  local START
  START="$(date +%s)"
  while true
  do
    if kubectl wait --timeout 1s -n "$CLUSTER_NAMESPACE" sgdbops "$DBOPS_NAME" \
      --for condition=Completed 2>/dev/null
    then
      echo "FAILED. $OP operation $CLUSTER_NAMESPACE.$DBOPS_NAME completed."
      echo
      echo "Job logs:"
      echo
      kubectl logs -n "$CLUSTER_NAMESPACE" \
        -l "app=StackGresDbOps,dbops-name=$DBOPS_NAME,db-ops=true" \
        --all-containers --ignore-errors --tail=-1
      echo
      return 1
    fi
    if kubectl wait --timeout 1s -n "$CLUSTER_NAMESPACE" sgdbops "$DBOPS_NAME" \
      --for condition=Failed 2>/dev/null
    then
      echo "SUCCESS. $OP operation $CLUSTER_NAMESPACE.$DBOPS_NAME failed."
      return
    fi
    if [ "$((START + E2E_TIMEOUT))" -le "$(date +%s)" ]
    then
      echo "SUCCESS. $OP operation $CLUSTER_NAMESPACE.$DBOPS_NAME did not complete after $E2E_TIMEOUT seconds"
      return
    fi
  done
}
