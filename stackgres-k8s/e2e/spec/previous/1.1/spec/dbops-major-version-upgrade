#!/bin/sh

. "$SPEC_PATH/abstract/dbops-major-version-upgrade"

e2e_test() {
  run_test "Checking that major version upgrade is working" check_major_version_upgrade_is_working

  run_test "Checking that major version upgrade check is working" check_major_version_upgrade_check_is_working
}

check_major_version_upgrade_is_working() {
  reset_cluster

  cat << EOF | kubectl create -f -
apiVersion: stackgres.io/v1
kind: SGDbOps
metadata:
  name: $DBOPS_NAME
  namespace: $CLUSTER_NAMESPACE
spec:
  sgCluster: $CLUSTER_NAME
  op: majorVersionUpgrade
  majorVersionUpgrade:
    postgresVersion: "$E2E_MAJOR_TARGET_POSTGRES_VERSION"
    sgPostgresConfig: postgresconf-for-major-version-upgrade
EOF

  wait_until eval '[ "$(kubectl get sgdbops -n "$CLUSTER_NAMESPACE" "$DBOPS_NAME" \
    --template "{{ .status.opRetries }}")" = "0" ]'
  kubectl get sgdbops -n "$CLUSTER_NAMESPACE" "$DBOPS_NAME" -o yaml > "$LOG_PATH/sgdbops.yaml"
  kubectl delete sgdbops -n "$CLUSTER_NAMESPACE" "$DBOPS_NAME"
  kubectl create -f "$LOG_PATH/sgdbops.yaml"

  check_major_version_upgrade

  kubectl delete sgdbops -n "$CLUSTER_NAMESPACE" "$DBOPS_NAME"
}

check_major_version_upgrade_check_is_working() {
  reset_cluster

  cat << EOF | kubectl create -f -
apiVersion: stackgres.io/v1
kind: SGDbOps
metadata:
  name: $DBOPS_NAME
  namespace: $CLUSTER_NAMESPACE
spec:
  sgCluster: $CLUSTER_NAME
  op: majorVersionUpgrade
  majorVersionUpgrade:
    postgresVersion: "$E2E_MAJOR_TARGET_POSTGRES_VERSION"
    sgPostgresConfig: postgresconf-for-major-version-upgrade
    check: true
EOF

  check_major_version_upgrade

  kubectl delete sgdbops -n "$CLUSTER_NAMESPACE" "$DBOPS_NAME"
}
